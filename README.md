Author: Lannin Nakai
ACP calculator explanation: Program for calculating brevet control open and close times. 
These open and close times are calculated based off of what kilometer the rider will be on when going 
through that section. Look at https://rusa.org/pages/acp-brevet-control-times-calculator to view the increments 
these times will assigned through. Some strange details of these calculations are that there should be relaxed control 
closing times when the location is less than 60 km away from the start. In general, try to avoid setting a control times 
within 20% of the total distance from the begining or the end of the brevet. The closing time from 0 km is one hour. 
There is a french version of the algorithm to deal with the difficulties of placing controls nearby the starting point of 
the brevet. The information is available in the above link but is not utilized here. Our table is given its apperance by
calc.html, then flask_brevets.py will take the arguments given through getJSON and push them through to acp_times.py, where 
the opening and closing times for the given control will be calculated, then AJAX will be used to display these results on 
the chart to the client.
Implementing MongoDB: This project introduces the convention of storing values in a MongoDB database instead of a python/html
convention of storing the data. The server will separately request data from the clients side as well as get data from this
database. The database allows us to store "important" values where the client can't touch them but we can.
Test Cases:
1) Entering a blank form will not trigger the submit button, as to allow the client to retype a proper entry to submit.
However, if the client tries to display an empty table that they have submitted, they will be met with an indexing error
or a blank screen. The blank display screen will prompt the client to go back and fix their entry.
2) After submitting one filled form, the client may decide to change the form a bit. To anticipate this, everytime the client
presses submit, the database is cleared, as to not clog the database with mismatched entries which would likely be confusing
to the client.
3) If the client leaves blank rows (in or out of sequence on the form) they will be ignored. All blank rows are ignored by 
display's output.
4) After submitting, then displaying a form: the client can go back to the home page and refill outthe form with no problems.
There should be no problems without hitting refresh too, but if any errors occur the client should
hit refresh. 
