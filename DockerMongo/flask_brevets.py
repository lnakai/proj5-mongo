"""
Replacement for RUSA ACP brevet time calculator
(see https://rusa.org/octime_acp.html)

"""
import os
import flask
from flask import request, redirect, url_for, Flask, render_template
import arrow  # Replacement for datetime, based on moment.js
import acp_times  # Brevet time calculations
import config
from pymongo import MongoClient

import logging

client = MongoClient(os.environ['DB_PORT_27017_TCP_ADDR'], 27017)

db = client.tododb
collection = db.samples

###
# Globals
###
app = flask.Flask(__name__)
CONFIG = config.configuration()
app.secret_key = CONFIG.SECRET_KEY

###
# Pages
###
@app.route("/_submit", methods=['GET','POST'])
def _submit():
    item_doc = {}
    if request.method=='POST':
        entry = request.form.getlist('entry[]')
        if entry != None:
            collection.remove({}) #corrects for user going back and forth
            olen = len(entry)
            i = 1
            while i < olen:
                if entry[i] != ",,":
                    enter = entry[i].split(',')
                    item_doc = {
                        'km' : enter[0],
                        'open' : enter[1],
                        'close' : enter[2]
                    }
                    collection.insert_one(item_doc)
                    i = i + 1
    return flask.render_template('calc.html')

@app.route("/_display", methods=['GET', 'POST'])
def _display():
    if request.method=='POST':
        if "display" in request.form:
            _items = collection.find()
            items = [item for item in _items]
            collection.remove({})
            if len(items) == 0:
                return ValueError
            return flask.render_template('display.html', items=items)

@app.route("/_calc_times", methods=['GET','POST'])
def _calc_times(km=999, distance=1000, begin_date='2000-02-20', begin_time='05:00'):
    """
    Calculates open/close times from miles, using rules
    described at https://rusa.org/octime_alg.html
    """
    km = request.args.get('km', 999, type=float)
    distance = request.args.get('brevet_dist_km', 1000, type=int)
    date = request.args.get('begin_date','2000-02-20')
    time = request.args.get('begin_time', '05:00')
    open_time = acp_times.open_time(km, distance, date + ' ' + time)
    close_time = acp_times.close_time(km, distance, date + ' ' + time)
    result = {"open": open_time, "close": close_time}
    return flask.jsonify(result=result)

@app.route("/")
@app.route("/index", methods=['GET','POST'])
def index():
    app.logger.debug("Main page entry")
    return flask.render_template('calc.html')


@app.errorhandler(404)
def page_not_found(error):
    app.logger.debug("Page not found")
    flask.session['linkback'] = flask.url_for("index")
    return flask.render_template('404.html'), 404


###############
#
# AJAX request handlers
#   These return JSON, rather than rendering pages.
#
###############

#############

app.debug = CONFIG.DEBUG
if app.debug:
    app.logger.setLevel(logging.DEBUG)

if __name__ == "__main__":
    print("Opening for global access on port {}".format(CONFIG.PORT))
    app.run(host= '0.0.0.0', port = CONFIG.PORT, debug = True)